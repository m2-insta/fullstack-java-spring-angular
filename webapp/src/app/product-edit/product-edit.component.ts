import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent {
  form = new FormGroup({
    name: new FormControl(''),
    price: new FormControl('')
  })

  createProduct = () => {
    console.log({
      form: this.form.value,
      form2: this.form,
      name: this.form.get('name')?.value,
      price: this.form.get('price')?.value
    })
  //  console.log({a: this.form.get('name').value, b: this.form.get('price').value})
  }
}
