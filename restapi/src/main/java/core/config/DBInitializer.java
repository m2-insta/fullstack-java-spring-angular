package core.config;

import javax.sql.DataSource;
import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DBInitializer {
    @Autowired
    private DataSource dataSource;

    @PostConstruct
    public void initialize() {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();

            // Drop table if exist and recreate table restaurant
            statement.execute("DROP TABLE IF EXISTS product;");
            statement.executeUpdate("CREATE TABLE product(" +
                    "id INTEGER PRIMARY KEY, " +
                    "name varchar(30), " +
                    "price INT not null, " +
                    "created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");

            statement.close();
            connection.close();
        } catch (SQLException error) {
            System.out.println("Couldn't initialize database.");
            System.out.println(error);
        }
    }
}