import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent {
  // test = () => {
  //   fetch("http://localhost:3000/api/v1/product", {
  //     method: "POST",
  //     body:
  //   })
  //   .then((response) => {console.log(response)})
  //   .catch((err) =>{console.log(err)})
  //   console.log("coucou")
  // }
  products = [
    {
      id: 1,
      name: 'toto',
      price: 10,
      createdAt: Date.now()
    },
    {
      id: 2,
      name: 'titi',
      price: 120,
      createdAt: Date.now()
    }
  ];
}
