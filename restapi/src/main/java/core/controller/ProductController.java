package core.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import core.repository.ProductRepository;
import core.model.Product;


@RestController
@RequestMapping("api/v1")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;
 

        // [POST] Create product
        @RequestMapping(value="/product", method=RequestMethod.POST)
        public void createProduct(@RequestBody Product product) {
            try {
                productRepository.save(new Product());
            } catch  (Exception error) {
                System.out.println("Couldn't create product");
                System.out.println(error);
            }
        }
    
        // [GET] Get all products
        @RequestMapping(value="/products", method=RequestMethod.GET)
        public ResponseEntity<List<Product>> getProducts() {
            try {
                List<Product> products = new ArrayList<Product>();
                products = productRepository.findAll();
    
                return new ResponseEntity<>(products, HttpStatus.OK);
            } catch  (Exception error) {
                System.out.println("Couldn't get all products");
                System.out.println(error);
              return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    
        // [PUT] Update an product by ID
        @RequestMapping(value="/product/{id}", method=RequestMethod.PUT)
        public ResponseEntity<Product> updateProductByID(@PathVariable("id") long id, @RequestBody Product product) {
            try {
                Optional<Product> productData = productRepository.findById(id);
                Product _product = productData.get();

                _product.setName(product.getName());
                _product.setPrice(product.getPrice());

                productRepository.save(_product);
    
                return new ResponseEntity<>(_product, HttpStatus.OK);
            } catch  (Exception error) {
                System.out.println("Couldn't update product");
                System.out.println(error);
              return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    
        // [DELETE] Delete an product by ID
        @RequestMapping(value="/product/{id}", method=RequestMethod.DELETE)
        public ResponseEntity<Product> deleteProductByID(@PathVariable("id") long id) {
            try {
                productRepository.deleteById(id);
    
                return new ResponseEntity<>(null, HttpStatus.OK);
            } catch  (Exception error) {
                System.out.println("Couldn't delete product");
                System.out.println(error);
                
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
}