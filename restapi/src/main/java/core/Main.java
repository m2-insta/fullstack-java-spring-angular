package core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@SpringBootApplication
public class Main
{
    public static void main( String[] args ) throws Exception
    { 
        // System.setProperty("java.net.useSystemProxies", "true");
        SpringApplication.run(Main.class, args);
    }
}