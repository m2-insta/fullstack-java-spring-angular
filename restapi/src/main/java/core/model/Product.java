package core.model;

import javax.persistence.*;

@Entity
@Table(name="product")
public class Product {

    
	@Id
    @Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;

	@Column(name = "name", nullable=false)
	private String name;

	@Column(name = "price", nullable=false)
	private Double price;

	@Column(name = "createdAt", nullable=false)
	private String createdAt;

    public Product () {}

    public Product (String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}